def count_first_letter(dictionary):
    #init empty dict
    count_dict = {}
    #retrieve first letter of the last name at position 0, check if in count_dict, if present incriment
    #by value of length of list of first name. else.. add first letter as key in count_dict
    for last_name, first_names in dictionary.items():
        first_letter = last_name[0]
        if first_letter in count_dict:
            count_dict[first_letter] += len(first_names)
        else:
            count_dict[first_letter] = len(first_names)
    return count_dict


dictionary = {'Stark': ['Ned', 'Robb', 'Sansa'], 'Snow': ['Jon'], 'Lannister': ['Jaime', 'Cersei', 'Tywin']}
result = count_first_letter(dictionary)
print(result)  # Output: {'S': 4, 'L': 3}
