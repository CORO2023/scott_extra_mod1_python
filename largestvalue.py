def max_key(dictionary):
    #find max value in dictionary values
    max_value = max(dictionary.values())
    #iterate over key pairs, see if equal to max value and return that key
    #if multiples return max_value
    for key, value in dictionary.items():
        if value == max_value:
            return key




dictionary1 = {1: 100, 2: 1, 3: 4, 4: 10}
result1 = max_key(dictionary1)
print(result1)  # Output: 1

dictionary2 = {'a': 100, 'b': 10, 'c': 1000}
result2 = max_key(dictionary2)
print(result2)  # Output: 'c'
