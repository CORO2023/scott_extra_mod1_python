def double_index(lst, index):
    #check if the index is valid by comparing it with the length of list
    #if the index is less than 0 or greater / equal to length of list it's not valid
    if index < 0 or index >= len(lst):
        return lst
    #if list is valid double the value by 2
    lst[index] *= 2
    return lst

list1 = [3, 8, -10, 12]
index1 = 2
result1 = double_index(list1, index1)
print(result1)  # Output: [3, 8, -20, 12]

list2 = [3, 8, -10, 12]
index2 = 6
result2 = double_index(list2, index2)
print(result2)  # Output: [3, 8, -10, 12]
