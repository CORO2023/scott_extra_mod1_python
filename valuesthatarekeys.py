def values_are_keys(dictionary):
    #filter out values that are keys, see if present, if key in dict include in list of values
    #return the list.
    return [value for value in dictionary.values() if value in dictionary.keys()]

dictionary1 = {1: 100, 2: 1, 3: 4, 4: 10}
result1 = values_are_keys(dictionary1)
print(result1)  # Output: [1, 4]

dictionary2 = {'a': 'apple', 'b': 'a', 'c': 100}
result2 = values_are_keys(dictionary2)
print(result2)  # Output: ['a']
