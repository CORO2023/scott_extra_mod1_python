def word_length_dict(word_list):
    #create pair from each word in dict, use the word as key and length as value. crate new dic with updates.
    return {word: len(word) for word in word_list}

word_list = ["apple", "cat", "pickle"]
result = word_length_dict(word_list)
print(result)  # Output: {'apple': 5, 'cat': 3, 'pickle': 6}
