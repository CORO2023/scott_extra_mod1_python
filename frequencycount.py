def frequency_count(lst):
    #init empty dict
    freq_dict = {}
    #iterate over each element
    for element in lst:
        #check if key in dict increase by 1
        if element in freq_dict:
            freq_dict[element] += 1
        #if not add to dict and set to 1
        else:
            freq_dict[element] = 1
        #output the dict
    return freq_dict



lst1 = ['apple', 'apple', 'cat', 1]
result1 = frequency_count(lst1)
print(result1)  # Output: {'apple': 2, 'cat': 1, 1: 1}

lst2 = [0, 0, 0, 0, 0]
result2 = frequency_count(lst2)
print(result2)  # Output: {0: 5}
