def sum_values(dictionary):
    #retreive all values of dictionar then sum all of them.
    return sum(dictionary.values())



dictionary = {'milk': 5, 'eggs': 2, 'flour': 3}
result = sum_values(dictionary)
print(result)  # Output: 10
