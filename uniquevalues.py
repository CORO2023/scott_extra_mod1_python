def unique_values(dictionary):
    #retreive values from dict with set remove duplicates
    unique_values = set(dictionary.values())
    #return the length of the unique values.
    return len(unique_values)


dictionary1 = {0: 3, 1: 1, 4: 1, 5: 3}
result1 = unique_values(dictionary1)
print(result1)  # Output: 2

dictionary2 = {0: 3, 1: 3, 4: 3, 5: 3}
result2 = unique_values(dictionary2)
print(result2)  # Output: 1

dictionary3 = {0: 3, 1: 6, 4: 9, 5: 1}
result3 = unique_values(dictionary3)
print(result3)  # Output: 4
