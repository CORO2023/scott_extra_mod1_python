def max_num(numbers):
    #max function returns largest number in the list
    return max(numbers)


numbers = [-10, 0, 20, 50, 75, 15]
result = max_num(numbers)
print(result)  # Output: 75
