def append_sum(lst):
    #iterate 3 times use _ since value isn't used in loop
    for _ in range(3):
    #calculate the sum of the last two runs by indexing the lst[-1] and lst[-2]
    #store in variable sum_last_two then append to lst and return modified list
        sum_last_two = lst[-1] + lst[-2]
        lst.append(sum_last_two)
    return lst

list1 = [1, 1, 2]
result1 = append_sum(list1)
print(result1)  # Output: [1, 1, 2, 3, 5, 8]
