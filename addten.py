def add_ten(dictionary):
#create new key value pair in dictionary and add 10 to each value.
    return {key: value + 10 for key, value in dictionary.items()}

dictionary = {1: 5, 2: 2, 3: 3}
result = add_ten(dictionary)
print(result)  # Output: {1: 15, 2: 12, 3: 13}
