def get_change():
    while True:
        try:
            cash_total = float(input("Enter the cash total: "))
            if cash_total < 0:
                raise ValueError("Cash total cannot be negative.")
            break
        except ValueError:
            print("Invalid input. Please enter a valid cash total.")

    total_cents = int(cash_total * 100)
    total_quarters = total_cents // 25
    total_cents %= 25
    total_dimes = total_cents // 10
    total_cents %= 10
    total_nickels = total_cents // 5
    total_cents %= 5
    total_pennies = total_cents

    print("Total Cents:", total_cents)
    print("Total Quarters:", total_quarters)
    print("Total Dimes:", total_dimes)
    print("Total Nickels:", total_nickels)
    print("Total Pennies:", total_pennies)




def test_get_change():
    # Test Case 1
    print("Test Case 1")
    print("Input: 0.87")
    get_change()
    print("Expected Output:")
    print("Total Cents: 2")
    print("Total Quarters: 3")
    print("Total Dimes: 1")
    print("Total Nickels: 0")
    print("Total Pennies: 2")
    print()

test_get_change()
