def greetings(names):
    #init empty list
    greetings_list = []
    #iterate over list of names
    for name in names:
    #concatenate hello and name in list
        greetings_list.append("Hello, " + name)
    #return list
    return greetings_list

names = ["Sam", "Jim", "Sophie"]
result = greetings(names)
print(result)  # Output: ["Hello, Sam", "Hello, Jim", "Hello, Sophie"]
