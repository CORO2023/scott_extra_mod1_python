DOUBLE INDEX:
Create a fxn named “double_index” with two parameters: a list and an index number. The fxn should double the value of the list element at the specified index and return the list with the doubled value.
If the index is not a valid index, the fxn should return the original list.

Input: ([3,8,-10,12], 2) => Output: [3,8,-20,12]
Input: ([3,8,-10,12], 6) => Output: [3,8,-10,12]


REMOVE MIDDLE:
Create a fxn named “remove_middle” which has three parameters: a list, a start number, an end number. The fxn should return a sub-list of the list containing all of the elements between the start and end indexes.

Input: ([4,8,15,16,23,42], 1, 3]) => Output: [4,23,42]

MORE THAN N
Create a fxn named “more_than_n” that has three parameters: a list, an item, and a number. The fxn should return “True” if the item appears more then the number of times specified. Otherwise, the fxn should return “False”.

Input: ([2,4,6,2,3,2,1,2], 2, 3) => Output: True
Input: ([2,4,6,2,3,2,1,2], 4, 3) => Output: False

MORE FREQUENT ITEM
Create a fxn named “most_frequent_item” that has three parameters: a list, a first item, a second item. Return either the first item or the second item depending on which occurs more often in the list. If the two items appear the same number of times, return the first item.

Input: ([2,3,3,2,3,2,3,2,3], 2, 3) => Output: 3
Input: ([2,3,3,2,3,2,3,2], 2, 3) => Output: 2

MIDDLE ITEM
Create a fxn named “middle_element” that has one parameter: a list. If there are an odd number of elements in the list, return the middle element. If there are an even number, return the average of the middle elements.

Input: ([5,2,-10,-4,4,5]) => Output: -7
Input: ([5,2,-10,-4,4,]) => Output: -10

APPEND SUM
Create a fxn named “append_sum” that has one parameter: a list. The fxn should add the last two elements of the list together and add the result to the end of the list. It should perform this process three times and then return the list.

Input: ([1,1,2]) => Output: [1,1,2,3,5,8]



COMBINE SORT
Create a fxn named “combine_sort” that has two parameters: two lists. The fxn should combine the two lists into a new list, sort the result, and then return the new list.

Input: ([4,10,2,5], [-10,2,5,10]) => Output: [-10,2,2,4,5,5,10,10]

APPEND SIZE
Create a fxn named “append_size” that has one parameter: a list. The fxn should add all of the numbers between 1 and the size of the list to the end of the list, and then return the list.

Input: ([23,42,108]) => Output: [23,42,108,1,2,3]

EVERY THREE NUMS
Create a fxn named “every_three_nums” that has one parameter: a number. The fxn should return a list of every third number between the number passed in and 100 (inclusive). If the number passed in is greater then 100, it should return and empty list.

Input: (91) => Output: [91, 94,97,100]
Input: (106) => Output: []


GREETINGS
Create a fxn named “greetings” that has one parameter: a list of names. In the fxn add the string “Hello” in front of each name and return the list.

Input: ([“Sam”, “Jim”, “Sophie”]) => Output: [“Hello, Owen”, “Hello, Jim”, “Hello, Sophie”]

MAX NUM
Create a fxn called “max_num” that has one parameter: a list of numbers. The fxn should return the largest number in the list

Input: ([-10,0,20,50,75,15]) => Output: 75

SAME VALUES
Create a fxn “same_values” that has two parameters: 2 lists of equal size. The fxn should return a list of indexes where there are equal values in each list.

Input: ([5,1,-10,3,3], [5,10,-10,3,5]) => Output: [0,2,3]

Bonus: try to solve this if you have 2 lists of unequal size

REVERSED LIST
Create a fxn named “reversed_list” that has 2 parameters: 2 lists. If the first list is the same as the second list reversed, the fxn should return “True”. If not, it should return “False”.

Input: ([1,2,3],[3,2,1]) => Output: True
Input: ([1,5,3],[3,2,1]) => Output: False

MAKE SPOONERISM
Create a fxn named “make_spoonerism” that has 2 parameters: 2 strings. The fxn should return the two words with the first letters switched.

Input: (“Hello”, “John”) => Output: Jello Hohn

SUM VALUES
Create a fxn named “sum_values” that takes one parameter: a dictionary. The fxn should return the sum of the values in the dictionary.

Input: ({‘milk’: 5, ‘eggs’: 2, ‘flour’: 3}) => Output: 10

EVEN KEYS
Create a fxn named “sum_even_keys” that takes in one parameter: a dictionary. The fxn should return the sum of all of the even keys.

Input: ({1: 5, 2: 2, 3: 3}) => Output: 2
Input: ({100: 5, 1000: 2, 10: 3}) => Output: 10

ADD TEN
Create a fxn named “add_ten” that takes in one parameter: a dictionary. The fxn should add 10 to every value in the dictionary and then return it.

Input: ({1: 5, 2: 2, 3: 3}) => Output: {1: 15, 2: 12, 3: 13}

VALUES THAT ARE KEYS
Create a fxn named “values_are_keys” that takes in one parameter: a dictionary. The fxn should return a list of values that also equal keys.

Input: ({1: 100, 2: 1, 3: 4, 4: 10}) => Output: [1, 4]
Input: ({‘a’: ‘apple’, ‘b’: ‘a’, ‘c’: 100}) => Output: [‘a’]

LARGEST VALUE
Create a fxn named “max_key” that takes in one parameter: a dictionary. The fxn should return the key associated with the largest value.

Input: ({1: 100, 2: 1, 3: 4, 4: 10}) => Output: 1
Input: ({‘a’: 100, ‘b’: 10, ‘c’: 1000}) => Output: c

WORD LENGTH DICTIONARY
Create a fxn named ”word_length_dict” that takes in one parameter: a list of strings. The fxn should return a dictionary where each key is a word in the word list and the value is the length of that word.

Input: ([“apple”, “cat”, “pickle”]) => Output: {‘apple’: 5, ‘cat’: 3, ‘pickle’: 6}





FREQUENCY COUNT
Create a fxn named “frequency_count” that takes in one parameter: a list. The fxn should return a dictionary containing the frequency of each element in the list.

Input: ([‘apple’, ‘apple’, ‘cat’, 1]) => Output: {‘apple’: 2, ‘cat’: 1, 1:1}
Input: ([0,0,0,0,0]) => Output: {0: 5}

UNIQUE VALUES
Create a fxn named “unique_values” that takes in one parameter: a dictionary. The fxn should return the number of unique values in the dictionary.

Input: ({0: 3, 1: 1, 4: 1, 5: 3}) => Output: 2
Input: ({0: 3, 1: 3, 4: 3, 5: 3}) => Output: 1
Input: ({0: 3, 1: 6, 4: 9, 5: 1}) => Output: 4

COUNT FIRST LETTER
Create a fxn named “count_first_letter” that takes in one parameter: a dictionary with a last name as a key and a list of first names as the value. The fxn should return a dictionary where each key is the first letter of the last name and the value is the number of people whose last name begins with that letter.

Input: ({‘Stark’: [‘Ned’, ‘Robb’, ‘Sansa], ‘Snow’: [‘Jon’], ‘Lannister’: [‘Jaime’, ‘Cersei’, ‘Tywin’]}) Output: {‘S’: 4, ‘L’: 3}


GET CHANGE
Create a fxn named “get_change” that gets user input for a cash total. Calculate the total number of Quarters / Dimes / Nickels / Pennies for the change exchange. The fxn should return a print of those calculations.
Note: make this a “hungry” fxn that gets the largest number of large coins and smallest number of small coins.
Note: validate the input to ensure proper input from the user, cover all edge case entries, do not allow breaking errors

Input: 1.49
Output:
Total Cents: 149
Total Quarters: 5
Total Dimes: 2
Total Nickels: 0
Total Pennies: 4
