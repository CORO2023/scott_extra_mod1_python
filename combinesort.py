def combine_sort(list1, list2):
    #create new list of list 1 and list 2
    combined_list = list1 + list2
    #sort in ascending order
    combined_list.sort()
    #return list
    return combined_list


list1 = [4, 10, 2, 5]
list2 = [-10, 2, 5, 10]
result = combine_sort(list1, list2)
print(result)  # Output: [-10, 2, 2, 4, 5, 5, 10, 10]
