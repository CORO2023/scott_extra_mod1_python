def make_spoonerism(word1, word2):
    #take first letter of word 2, and concatenate with word 1 [1:] tells it to add to end, add a space with ""
    #do the same with first letter of word 1 to word 2. Voila ..
    return word2[0] + word1[1:] + " " + word1[0] + word2[1:]







word1 = "Hello"
word2 = "John"
result = make_spoonerism(word1, word2)
print(result)  # Output: "Jello Hohn"
