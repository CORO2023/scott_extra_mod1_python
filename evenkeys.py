def sum_even_keys(dictionary):
    #check if key is even with modulo (key % 2 == 0), returns the even keys from dictionary then
    # then uses sum to calculate value of even keys
    return sum(key for key in dictionary.keys() if key % 2 == 0)



dictionary1 = {1: 5, 2: 2, 3: 3}
result1 = sum_even_keys(dictionary1)
print(result1)  # Output: 2

dictionary2 = {100: 5, 1000: 2, 10: 3}
result2 = sum_even_keys(dictionary2)
print(result2)  # Output: 1110
