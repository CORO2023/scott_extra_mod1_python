def more_than_n(lst, item, number):
    #count number of items in the list
    count = lst.count(item)
    #compare count with number
    return count > number

list1 = [2, 4, 6, 2, 3, 2, 1, 2]
item1 = 2
number1 = 3
result1 = more_than_n(list1, item1, number1)
print(result1)  # Output: True

list2 = [2, 4, 6, 2, 3, 2, 1, 2]
item2 = 4
number2 = 3
result2 = more_than_n(list2, item2, number2)
print(result2)  # Output: False
