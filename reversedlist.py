def reversed_list(list1, list2):
    #if list 1 and 2 are equal (google python slicing technique) it reverses syntax in list 2
    #if they are equal they are returned true
    return list1 == list2[::-1]





list1 = [1, 2, 3]
list2 = [3, 2, 1]
result1 = reversed_list(list1, list2)
print(result1)  # Output: True

list3 = [1, 5, 3]
list4 = [3, 2, 1]
result2 = reversed_list(list3, list4)
print(result2)  # Output: False
