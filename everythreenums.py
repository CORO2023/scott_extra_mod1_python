def every_three_nums(number):
    #if greater than 100 return empty list
    if number > 100:
        return []
    # generate seuquence of number with range. Using number as start, up to and including 100(101 is end) every 3 numbers.
    else:
        return list(range(number, 101, 3))


result1 = every_three_nums(91)
print(result1)  # Output: [91, 94, 97, 100]

result2 = every_three_nums(106)
print(result2)  # Output: []
