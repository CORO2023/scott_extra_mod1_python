def remove_middle(lst, start, end):
    new_list = []
    for i in range(start, end + 1):
        new_list.append(lst[i])
    return new_list
print(remove_middle([4, 8, 15, 16, 23, 42], 1, 3))


#second method (fixed how I did it originally)

def remove_middle2(lst, start, end):
    return lst[start:end+1:]
print(remove_middle2([4, 8, 15, 16, 23, 42], 1, 3))
