def append_size(lst):
    #calc len store in variable size
    size = len(lst)
    #iterate from one to size of list.
    for i in range(1, size+1):
    #append the current number of i
        lst.append(i)
    #modify list / return it
    return lst

list1 = [23, 42, 108]
result1 = append_size(list1)
print(result1)  # Output: [23, 42, 108, 1, 2, 3]


#Method 2
def append_size(list):
    size = len(list)
    list.extend(range(1, size+1))
    return list

list1 = [23, 42, 108]
result1 = append_size(list1)
print(result1)  # Output: [23, 42, 108, 1, 2, 3]


#Method 3

def append_size(lst):
    return lst + [i for i in range(1, len(lst) + 1)]

list1 = [23, 42, 108]
result1 = append_size(list1)
print(result1)  # Output: [23, 42, 108, 1, 2, 3]
