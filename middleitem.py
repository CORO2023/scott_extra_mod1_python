def middle_element(lst):
    #determine lengeth of lst
    length = len(lst)
    #if list is odd there is 1 middle element stored in middle_index
    if length % 2 == 1:
        middle_index = length // 2
    #return element at middle index of the lst
        return lst[middle_index]
    else:
    #if length is even there are 2 middle elements
    #calc first element w/ division store in middle_index_1
        middle_index_1 = length // 2 - 1

        middle_index_2 = length // 2
    #return the average of the two indexes and divide by 2
        return (lst[middle_index_1] + lst[middle_index_2]) / 2


list1 = [5, 2, -10, -4, 4, 5]
result1 = middle_element(list1)
print(result1)  # Output: -7

list2 = [5, 2, -10, -4, 4]
result2 = middle_element(list2)
print(result2)  # Output: -10
