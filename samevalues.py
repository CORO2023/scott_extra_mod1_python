def same_values(list1, list2):
    #create a list to store the common indexes
    common_indexes = []
    #determine the minimal length between two lists using the min() function store in min_len
    min_len = min(len(list1), len(list2))

    #iterate over the range up to min_len
    for i in range(min_len):
        #if index in list 1 and list two are equal
        if list1[i] == list2[i]:
            #add to common_indexes list
            common_indexes.append(i)

    return common_indexes

list1 = [5, 1, -10, 3, 3]
list2 = [5, 10, -10, 3, 5]
result = same_values(list1, list2)
print(result)  # Output: [0, 2, 3]
