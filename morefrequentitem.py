def most_frequent_item(lst, item1, item2):

    #count items in lists
    count_item1 = lst.count(item1)
    count_item2 = lst.count(item2)

    #if count of 1 is great or equal return.. else go to 2
    if count_item1 >= count_item2:
        return item1
    else:
        return item2

list1 = [2, 3, 3, 2, 3, 2, 3, 2, 3]
item1 = 2
item2 = 3
result1 = most_frequent_item(list1, item1, item2)
print(result1)  # Output: 3

list2 = [2, 3, 3, 2, 3, 2, 3, 2]
item3 = 2
item4 = 3
result2 = most_frequent_item(list2, item3, item4)
print(result2)  # Output: 2
